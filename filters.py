#!/usr/bin/env python3

###############
# Code developed by Luca Baronti
# Please refer to the repository for further information 
# https://gitlab.com/luca.baronti/explog
###############

class Filter(object):
	def __init__(self):
		pass
	def is_good(self, entry):
		return True

class FilterUser(Filter):
	def __init__(self, user):
		super(FilterUser, self).__init__()
		self.user=user

	def is_good(self, entry):
		return entry['user']==self.user

class FilterTime(Filter):
	def __init__(self, datetime):
		super(FilterTime, self).__init__()
		date_time = datetime.split('-')
		date= date_time[0]
		if len(date_time)>1:
			time=date_time[1]
		else:
			time=None
		dd_mm_yy = date.split('/')
		self.dd, self.MM, self.yy = 1, 1, None
		if len(dd_mm_yy)>2:
			self.dd=int(dd_mm_yy[0])
			del dd_mm_yy[0]
		if len(dd_mm_yy)>1:
			self.MM=int(dd_mm_yy[0])
			del dd_mm_yy[0]
		self.yy=int(dd_mm_yy[0])
		self.hh, self.mm, self.ss = 0, 0, 0
		if time!=None:
			hh_mm_ss = time.split(':')
			if len(hh_mm_ss)>2:
				self.ss=int(hh_mm_ss[-1])
				del hh_mm_ss[-1]
			if len(hh_mm_ss)>1:
				self.mm=int(hh_mm_ss[-1])
				del hh_mm_ss[-1]
			self.hh=int(hh_mm_ss[0])
		
class FilterFromTime(FilterTime):
	def __init__(self, datetime):
		super(FilterFromTime, self).__init__(datetime)

	def is_good(self, entry):
		dd_MM_yy = [int(x) for x in entry['date'].split('/')]
		dd, MM, yy = dd_MM_yy[0], dd_MM_yy[1], dd_MM_yy[2]
		if self.yy>yy:
			return False
		if self.MM!=None and self.MM>MM:
			return False
		if self.dd!=None and self.dd>dd:
			return False
		hh_mm_ss = [int(x) for x in entry['time'].split(':')]
		hh, mm, ss = hh_mm_ss[0], hh_mm_ss[1], hh_mm_ss[2]
		if self.hh!=None and self.hh>hh:
			return False
		if self.mm!=None and self.mm>mm:
			return False
		if self.ss!=None and self.ss>ss:
			return False
		return True

class FilterToTime(FilterTime):
	def __init__(self, datetime):
		super(FilterToTime, self).__init__(datetime)

	def is_good(self, entry):
		dd_MM_yy = [int(x) for x in entry['date'].split('/')]
		dd, MM, yy = dd_MM_yy[0], dd_MM_yy[1], dd_MM_yy[2]
		if self.yy<yy:
			return False
		if self.MM!=None and self.MM<MM:
			return False
		if self.dd!=None and self.dd<dd:
			return False
		hh_mm_ss = [int(x) for x in entry['time'].split(':')]
		hh, mm, ss = hh_mm_ss[0], hh_mm_ss[1], hh_mm_ss[2]
		if self.hh!=None and self.hh<hh:
			return False
		if self.mm!=None and self.mm<mm:
			return False
		if self.ss!=None and self.ss<ss:
			return False
		return True

def select_filters(args):
	filters=[]
	if args.user!=None:
		filters+=[FilterUser(args.user[0])]
	if args.from_time!=None:
		filters+=[FilterFromTime(args.from_time[0])]
	if args.to_time!=None:
		filters+=[FilterToTime(args.to_time[0])]
	return filters


