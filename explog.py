#!/usr/bin/env python3

###############
# Code developed by Luca Baronti
# Please refer to the repository for further information 
# https://gitlab.com/luca.baronti/explog
###############

import sys, os, argparse, getpass, datetime, json
import filters


LOG_MAIN_DIR_NAME 	= ".explog"
LOG_MAIN_FILE_NAME 	= "history.json"

class History(object):
	def __init__(self):
		if not os.path.exists(LOG_MAIN_DIR_NAME):
			os.makedirs(LOG_MAIN_DIR_NAME)
		self.fullpath = os.path.join(LOG_MAIN_DIR_NAME,LOG_MAIN_FILE_NAME)
		if os.path.isfile(self.fullpath):
			f=open(self.fullpath)
			# file_text = f.read()
			self.data = json.load(f)
			f.close()
		else:
			# the file doesn't exists, it must be created
			self.data = {}

	def add_entry(self,user,timestamp,message):
		key = str(timestamp)+'-'+user
		if key in self.data:
			raise ValueError("Error 100")
		dt = datetime.datetime.fromtimestamp(timestamp)
		vals={}
		vals['user']		= user
		vals['date']			= '/'.join([self.to2digits(x) for x in [dt.day,dt.month,dt.year]])
		vals['time']		= ':'.join([self.to2digits(x) for x in [dt.hour,dt.minute,dt.second]])
		vals['message']	= message
		self.data[key] 	= vals

	def to2digits(self,val):
		val=str(val)
		if len(val)==1:
			val='0'+val
		return val

	def breakdown_key(self, key):
		vals=key.split('-')
		if key.count('-')>1:
			vals[1]='-'.join(vals[1:])
		return float(vals[0]),vals[1]

	def entry2str(self, entry):
		return entry['date']+' '+entry['time']+' <'+entry['user']+'> "'+entry['message']+'"'

	def is_entry_filtered(self, k, filters2apply):
		ret = True
		for f in filters2apply:
			ret = ret and f.is_good(self.data[k])
		return not ret

	def print_first_entries(self, n_entries, filters2apply=[]):
		first_entries = []
		for k in self.data.keys():
			if self.is_entry_filtered(k,filters2apply):
				continue
			timestamp, _ = self.breakdown_key(k)
			if len(first_entries)<n_entries:
				first_entries+=[k]
			else:
				for i in range(n_entries):
					if float(timestamp)<float(self.breakdown_key(first_entries[i])[0]):
						first_entries[i]=k
						break
		first_entries.sort()
		for e in first_entries:
			print(self.entry2str(self.data[e]))

	def print_last_entries(self, n_entries, filters2apply=[]):
		last_entries = []
		for k in self.data.keys():
			timestamp, _ = self.breakdown_key(k)
			if len(last_entries)<n_entries:
				last_entries+=[k]
			else:
				for i in range(n_entries):
					if float(timestamp)>float(self.breakdown_key(last_entries[i])[0]):
						last_entries[i]=k
						break
		last_entries.sort()
		for e in last_entries:
			print(self.entry2str(self.data[e]))

	def save(self):
		f=open(self.fullpath,'w')
		f.write(json.dumps(self.data))
		f.close()

if __name__=='__main__':
	parser = argparse.ArgumentParser(description="Experiments logging system.")
	parser.add_argument('message', nargs='*', metavar='message', type=str, default='',
																	help='Message to be added.')
	parser.add_argument('-v','--visualise_logs',  action='store_true',
                    help='Visualise logs in a GUI.')
	parser.add_argument('-head',  metavar='n_entries',  nargs='?', type=int, default=0,
                    help='Show the first messages.')
	parser.add_argument('-tail',  metavar='n_entries', nargs='?', type=int, default=0,
                    help='Show the last messages.')
	parser.add_argument('-u','--user',  metavar='user', nargs=1, type=str,
                    help='Filter results by user.')
	parser.add_argument('-from_time',  metavar='datetime', nargs=1, type=str,
                    help='Filter results FROM a certain date and/or time. Theformat is dd/MM/yy-hh:mm:ss where most of the fields are optional. Using -from 2019 or -from 11/2019 for instance form a valid command.')
	parser.add_argument('-to_time',  metavar='datetime', nargs=1, type=str,
                    help='Filter results TO a certain date and/or time. Theformat is dd/MM/yy-hh:mm:ss where most of the fields are optional. Using -from 2019 or -from 11/2019 for instance form a valid command.')
	args = parser.parse_args()

	filters2apply = filters.select_filters(args)

	if args.visualise_logs:
		print("Unsupported feature")
		sys.exit(0)

	if args.head!=0:
		if args.head==None:
			args.head=5
		if args.head>0:
			history = History()
			history.print_first_entries(args.head, filters2apply)
		sys.exit(0)

	if args.tail!=0:
		if args.tail==None:
			args.tail=5
		if args.tail>0:
			history = History()
			history.print_last_entries(args.tail, filters2apply)
		sys.exit(0)

	if args.message!='':
		# need to add a message
		user = getpass.getuser()
		timestamp = datetime.datetime.now().timestamp()
		message = ' '.join(args.message)
		history = History()
		history.add_entry(user,timestamp,message)
		history.save()
		print("Entry "+str(timestamp)+'-'+user+" added")
